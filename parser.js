var path  = require('path');
var fs    = require('fs');
var utils = require('./utils');

function getFiles(dir,filter,depth){
  var matchedFiles = [];
  if (!fs.existsSync(dir)){
    console.log("no dir ",dir);
    return;
  }

  var files=fs.readdirSync(dir);
  for (var i=0;i<files.length;i++) {
    var filename=path.join(dir,files[i]);
    var stat = fs.lstatSync(filename);
    if ((depth>0 || depth < 0) && stat.isDirectory()) {
      if (filename.endsWith('node_modules') || filename.match('node_modules/[^/]*$')) {
        matchedFiles = matchedFiles.concat(getFiles(filename,filter,depth-1));
      }
    }
    else if (filename.match(filter)) {
      matchedFiles.push(filename);
    };
  };
  return matchedFiles;
};

function appendLicenseFinal(package) {
  package.licenseFinal = package.license || package.licenses || 'files';
  if (package.licenseFinal == 'files') {
    package.licenseFiles = getLicenseFiles(package);
  }
  return package;
}

function getLicenseFiles(package) {
  dir = package.path.substring(0, package.path.lastIndexOf('/'));
  return getFiles(dir,/.*license.*/i,0);
}

function printLicense(package) {
  var id = package.name + '@' + package.version;
  if (package.licenseFinal == 'files') {
    if (package.licenseFiles.length > 0) {
      console.log('possible license files for',id,'in',dir);
      package.licenseFiles.forEach(file => console.log(file));
    } else {
      console.log(id,': undefined');
    }
  } else {
    console.log(id,':',JSON.stringify(package.licenseFinal));
  }
}

files = getFiles(process.argv[2],/package.json$/,-1);
packages = [];

files.forEach(function(file) {
  var package = JSON.parse(fs.readFileSync(file, 'utf8'));
  package.path = file;
  package = appendLicenseFinal(package);
  packages.push(package);
});

packages = utils.sortByKeys(packages, ['name', 'version']);
packages = utils.removeDuplicates(packages, ['name', 'version']);

if (process.argv[3] && process.argv[3] == 'licenses') {
  licenses = [];
  packages.forEach(function(package) {
    container = {license: package.licenseFinal};
    licenses.push(container);
  });
  licenses = utils.sortByKeys(licenses, ['license']);
  licenses = utils.removeDuplicates(licenses,['license']);
  licenses.forEach(license => console.log(license.license));
} else {
  if (process.argv[3] && process.argv[3] == 'files') {
    packages = packages.filter(package => package.licenseFinal == 'files')
  }
  packages.forEach(package => printLicense(package));
}
