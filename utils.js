exports.sortByKeys = function(array, keys) {
  return array.sort(function(a, b) {
    var keysLength = keys.length;
    for (var i = 0; i < keys.length; i++) {
      if (a[keys[i]] < b[keys[i]]) {
        return -1;
      } else if (a[keys[i]] > b[keys[i]]) {
        return 1;
      }
    }
    return 0;
  });
}

exports.removeDuplicates = function(array, keys)  {
  return array.filter((entry, index, self) =>
    index === self.findIndex((t) => {
      var keysLength = keys.length;
      for (var i = 0; i < keys.length; i++) {
        if (!(t[keys[i]] === entry[keys[i]])) {
          return false;
        }
      }
      return true;
    })
  );
}
